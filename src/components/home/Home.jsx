import React, { useEffect } from "react";
import { Button, Typography } from "@material-ui/core";
import { styled } from "@material-ui/core/styles/styled";
import { useState } from "react";
import counterUp from "../../redux/actionCreators/honeAction";
import homeReducer from "../../redux/reducers/homeReducer/homeReducer";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

function Home(props) {
  const [count, setCount] = useState(0);

  useEffect(() => {
    // Update the document title using the browser API
    console.log("normal useEffect!!!");
  }, []);

  // const counterInc = ()=>{
  //   for(let i = 0; i < 100; i++){
  //     let tt = setInterval((i)=>{
  //       console.log(i)
  //     },1000)
  //     if(i == 100){
  //       tt.clearInterval()
  //     }

  //   }
  // }



  const counter = (limit)=>{
  
    let count= 0
    
    let id = setInterval(()=>{
    if(count === (limit-1)){
        clear()
    }
    let value = (count+=1);
    setCount(value)

    },10)
    
    let clear = ()=>{
    clearInterval(id)
    }
    
    }

  useEffect(() => {
    // Update the document title using the browser API
    console.log("count useEffect!!!");
  }, [count]);

  return (
    <main>
      <Typography variant="h1" align="center" color="primary">
        Counter
      </Typography>
      <Typography variant="h3" align="center">
        {/* {props.counter} */}
        {count}
      </Typography>

      <div style={{ width: "50%", margin: "auto" }}>
        <Button
          variant="outlined"
          fullWidth
          color="primary"
          onClick={ ()=>{
            counter(500)
          }
            
            // props.counterUp(props.counter);
          }
        >
          Click Me
        </Button>

        <div className="mt-3">
          <Button
            variant="outlined"
            fullWidth
            onClick={() => {
              setCount(0)
              // props.counterUp(props.counter);
            }}
          >
            Reset
          </Button>
        </div>
      </div>
    </main>
  );
}

function mapStateToProps(state) {
  return {
    counter: state.HomeReducer.counter,
  };
  console.log("?????????/", state);
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      counterUp,
    },
    dispatch
  );
}

// export default Home;

export default connect(mapStateToProps, mapDispatchToProps)(Home);
