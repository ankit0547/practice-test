import "./App.css";
import Home from "./components/home/Home";
import Header from "./components/common/header/Header";
import Footer from "./components/common/footer/Footer";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./components/theme/theme";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Header />
        <Home />
        <Footer />
      </div>
    </ThemeProvider>
  );
}

export default App;
