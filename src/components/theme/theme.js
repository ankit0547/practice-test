import { createMuiTheme , responsiveFontSizes} from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';
import { blue, red } from '@material-ui/core/colors';

const customTheme = createMuiTheme({
  palette: {
    primary: {
      main: blue[900],
    },
    secondary: {
      main: green[500],
    },
  },
});

const theme = responsiveFontSizes(customTheme);

export default theme;
