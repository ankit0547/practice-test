import React from "react";
import "./Header.css";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import { fade, makeStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'


const useStyles = makeStyles({
  root: {
    backgroundColor: 'red',
    color: props => props.color,
  },
  brandLogo :{
   flexGrow:'1',
    
  }
  
});


function Header() {
  const classes = useStyles()
  return (
    
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.brandLogo} noWrap>
            Material-UI
          </Typography>

           <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            className={classes.matToolBar}
          >
            <MenuIcon/>
          </IconButton>

          {/* <Menu
        id="simple-menu"
       
        keepMounted
      
      >
        <MenuItem >Profile</MenuItem>
        <MenuItem >My account</MenuItem>
        <MenuItem >Logout</MenuItem>
      </Menu> */}

        </Toolbar>
      </AppBar>
   
  );
}

export default Header;
